const express = require('express')
const path = require('path')
const app = express()

app.use(express.static(path.join(__dirname, "../front/build")))
//sapp.get('../', express.static('/'))

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "../front/build"))
})
//app.get('/public', express.static('./'));
app.get("/json", (req, res)=>{
  res.json({name: "John"})
})

app.listen(2000)